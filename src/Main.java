import controller.Controller;

public class Main {
	private static Controller controller;
	public static void main(String[] args) {
		controller = new Controller();
		System.out.println("Task 1:");
		controller.printDistinctDays();
		
		System.out.println("Task 2:");
		controller.printAllMap("ActivitiesMap.txt");
		
		System.out.println("Task 3:");
		controller.printDayMaps("ActivitiesPerDaysMap.txt");
		
		System.out.println("Task 4:");
		controller.printDurationStream("ActivitiesDurationMap.txt", 10);
		
		System.out.println("Task 5:");
		controller.printSampleDurationStream("ActivityFilter.txt", 5, 90);
		
	}
}
