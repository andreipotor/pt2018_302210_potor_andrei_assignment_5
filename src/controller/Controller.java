package controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import model.MonitoredData;
import model.Date;
import model.Hour;

public class Controller {
	private ArrayList<MonitoredData> data;
	
	public Controller(){
		String fileName = "Activities.txt";
		List<String> input = null;
		data = new ArrayList<MonitoredData>();

		//read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			input = stream.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// importing the data
		input.forEach(item->{
			String[] a = tokenize(item);
			data.add(new MonitoredData(getDate(a[0]), getHour(a[1]), getDate(a[2]), getHour(a[3]), a[4]));
		});
	}
	
	// ----------------------------------------------------- task 5 -----------------------------------------------------

	public void printSampleDurationStream(String address, int minuteFilter, int percent){		
		// used for comparison in the filter
		Hour m = new Hour(0, minuteFilter, 0);
		
		HashMap<String, ArrayList<MonitoredData>> map = new HashMap<String, ArrayList<MonitoredData>>();
		
		// splitting the data into a hash map based on activity - with value = an array list of data entries 
		data.stream().forEach(item -> {
			if(map.get(item.getActivity()) == null)
				map.put(item.getActivity(), new ArrayList<MonitoredData>());
			map.get(item.getActivity()).add(item);
		});
		
		// list for storing the results
		ArrayList<String> res = new ArrayList<String>();
		
		// filtering the list for each activity and comparing sizes
		map.entrySet().stream().forEach(item -> {
			Stream<MonitoredData> aux = item.getValue().stream().filter(item2->{
				if(item2.getDuration().subtract(m).isNegative())
					return true;
				else
					return false;
			});
			
			if(aux.count() * 100.0f / item.getValue().size() >= percent)
				res.add(item.getKey());
		});
		
		// writing the results
		printList(res, address);
	}
	
	private void printList(ArrayList<String> res, String address){
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(address));
			
			for(String s: res)
				writer.write(s + "\r\n");
			
			writer.close();
			
			System.out.println("Wrote the results in the file \"" + address + "\"");
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// ----------------------------------------------------- task 4 -----------------------------------------------------
	
	public void printDurationStream(String address, int hourFilter){
		HashMap<String, Hour> map = new HashMap<String, Hour>();
		
		// making the map
		data.stream().forEach(item -> {
			if(map.get(item.getActivity()) == null)
				map.put(item.getActivity(), item.getDuration());
			else
				map.put(item.getActivity(), 
						map.get(item.getActivity()).add(item.getDuration()));
		});
		
		// used for comparison in the filter
		Hour h = new Hour(hourFilter,0,0);
		
		// filtering the stream
		Stream<Map.Entry<String,Hour>> aux = map.entrySet().stream().filter(item -> {
			if(item.getValue().subtract(h).isNegative())
				return false;
			return true;
		});
		
		//printing the new filtered stream
		printStream(aux, address);
	}
	
	private void printStream(Stream<Map.Entry<String,Hour>> aux, String address){
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(address));
			
			aux.forEach(item -> {
				try {
					writer.write(item.getKey() + ": " + item.getValue() + "\r\n");
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			
			writer.close();
			
			System.out.println("Wrote the duration filter in the file \"" + address + "\"");
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	// ----------------------------------------------------- task 3 -----------------------------------------------------	
	
	public HashMap<String, Integer> makeDayMap(Date d){
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		
		// making the map
		data.stream().forEach(item -> {
			if(item.getStartTime().date.equals(d)){
				if(map.get(item.getActivity()) == null)
					map.put(item.getActivity(), 1);
				else
					map.put(item.getActivity(), map.get(item.getActivity()) + 1);
			}
		});
		
		return map;
	}
	
	public void printDayMaps(String address){
		// building our days list
		ArrayList<Date> days = new ArrayList<Date>();
		data.stream().forEach(item -> {
			if(!days.contains(item.getStartTime().date))
				days.add(item.getStartTime().date);
			if(!days.contains(item.getEndTime().date))
				days.add(item.getEndTime().date);
		});
		
		HashMap<Date, HashMap<String, Integer>> dayMaps = new HashMap<Date, HashMap<String, Integer>>();
		
		for(Date d: days)
			dayMaps.put(d, makeDayMap(d));
		
		printMap(dayMaps, address);
	}

	
	// ----------------------------------------------------- task 2 -----------------------------------------------------
	
	public void printAllMap(String address){
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		
		// making the map
		data.stream().forEach(item -> {
			if(map.get(item.getActivity()) == null)
				map.put(item.getActivity(), 1);
			else
				map.put(item.getActivity(), map.get(item.getActivity()) + 1);
		});
		
		// writing to file
		printMap(map, address);

	}

	// ----------------------------------------------------- task 1 -----------------------------------------------------
	
	public void printDistinctDays(){
		// building our days list
		ArrayList<Date> days = new ArrayList<Date>();
		data.stream().forEach(item -> {
			if(!days.contains(item.getStartTime().date))
				days.add(item.getStartTime().date);
			if(!days.contains(item.getEndTime().date))
				days.add(item.getEndTime().date);
		});
		
		System.out.println("Distinct days: " + days.size());
		System.out.println();
	}
	
	// ----------------------------- aux methods -----------------------------------
	
	private void printMap(HashMap map, String address){
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(address));
			
			for(Object s: map.keySet())
				writer.write(s + ": " + map.get(s) + "\r\n");
			
			writer.close();
			
			System.out.println("Wrote the map in the file \"" + address + "\"");
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private int[] getHour(String s){
		int[] ret = new int[3];
		String[] aux;
		
		aux = s.split(":");
		
		ret[0] = Integer.valueOf(aux[0]);
		ret[1] = Integer.valueOf(aux[1]);
		ret[2] = Integer.valueOf(aux[2]);
		
		return ret;
	}
	
	private int[] getDate(String s){
		int[] ret = new int[3];
		String[] aux;
		
		aux = s.split("-");
		
		ret[0] = Integer.valueOf(aux[0]);
		ret[1] = Integer.valueOf(aux[1]);
		ret[2] = Integer.valueOf(aux[2]);
		
		return ret;
	}
	
	private String[] tokenize(String s){
		String[] ret = new String[5];
		String[] aux;
		String[] start;
		String[] end;
		
		aux = s.split("\t\t");
		start = aux[0].split(" ");
		end = aux[1].split(" ");
		
		ret[0] = start[0];
		ret[1] = start[1];
		ret[2] = end[0];
		ret[3] = end[1];
		ret[4] = aux[2].replace("\t", "");
		return ret;
	}
	
}
