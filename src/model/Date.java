package model;

public class Date {
	public int year;
	public int month;
	public int day;
	
	public Date(){
		year = 0;
		month = 0;
		day = 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj.getClass() != this.getClass())
			return false;
		Date aux = (Date) obj;
		
		if(aux.year != this.year)
			return false;
		if(aux.month != this.month)
			return false;
		if(aux.day != this.day)
			return false;
		
		return true;
	}
	
	
	@Override
	public String toString() {
		return year + "-" + month + "-" + day;
	}
	
	@Override
    public int hashCode(){
        return year * 10000 + month * 100 + day;
    }
}
