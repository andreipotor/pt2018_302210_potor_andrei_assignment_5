package model;

public class Hour {
	public int hour;
	public int minute;
	public int second;
	
	public Hour(int hour, int minute, int second){
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}
	
	public Hour(Hour h){
		this.hour = h.hour;
		this.minute = h.minute;
		this.second = h.second;
	}
	
	public Hour(){
		this.hour = 0;
		this.minute = 0;
		this.second = 0;
	}
	
	public void set(int hour, int minute, int second){
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}
	
	public void set(Hour hour){
		this.hour = hour.hour;
		this.minute = hour.minute;
		this.second = hour.second;
	}
	
	public Hour subtract(Hour h){
		Hour res = new Hour();
		res.hour = hour;
		res.minute = minute;
		res.second = second;
		
		if(res.second - h.second >= 0){
			res.second -= h.second;
		} else {
			res.second += 60 - h.second;
			res.minute--;
		}
		
		if(res.minute - h.minute >= 0){
			res.minute -= h.minute;
		} else {
			res.minute += 60 - h.minute;
			res.hour--;
		}
		
		res.hour -= h.hour;
		
		return res;
	}
	
	public Hour add(Hour h){
		Hour res = new Hour();
		res.hour = hour;
		res.minute = minute;
		res.second = second;
		
		if(res.second + h.second < 60){
			res.second += h.second;
		} else {
			res.second += h.second - 60;
			res.minute++;
		}
		
		if(res.minute + h.minute < 60){
			res.minute += h.minute;
		} else {
			res.minute += h.minute - 60;
			res.hour++;
		}
		
		res.hour += h.hour;
		
		return res;
	}
	
	public boolean isNegative(){
		if(hour < 0)
			return true;
		if((hour == 0) && (minute < 0))
			return true;
		if((hour == 0) && (minute == 0) && (second < 0))
			return true;
		return false;
	}
	
	public int toSeconds(){
		return hour * 3600 + minute * 60 + second;
	}
	
	@Override
	public String toString() {
		String hour = String.valueOf(this.hour);
		String minute = String.valueOf(this.minute);
		String second = String.valueOf(this.second);
		if(minute.length() == 1)
			minute = "0" + minute;
		if(second.length() == 1)
			second = "0" + second;
		return hour + ":" + minute + ":" + second;
	}
}
