package model;

public class MonitoredData {
	private Time startTime;
	private Time endTime;
	private String activity;
	
	public MonitoredData(int[] startYear, int[] startHour, int[] endYear, int[] endHour, String activity){
		startTime = new Time();
		endTime = new Time();
		
		startTime.date.year = startYear[0];
		startTime.date.month = startYear[1];
		startTime.date.day = startYear[2];
		
		startTime.hour.hour = startHour[0];
		startTime.hour.minute = startHour[1];
		startTime.hour.second = startHour[2];
		
		endTime.date.year = endYear[0];
		endTime.date.month = endYear[1];
		endTime.date.day = endYear[2];
		
		endTime.hour.hour = endHour[0];
		endTime.hour.minute = endHour[1];
		endTime.hour.second = endHour[2];
		
		this.activity = activity;
	}
	
	public Hour getDuration(){
		Hour h = endTime.hour.subtract(startTime.hour);
		if(h.isNegative()) // if the activity stretches on two days we add 24 hours to the difference
			h = h.add(new Hour(24,0,0));
		return h;
	}
	
	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	@Override
	public String toString() {
		return startTime.toString() + " " + endTime.toString() + " " + activity;
	}
	
}
