package model;

public class Time {
	public Date date;
	public Hour hour;
	
	public Time(){
		date = new Date();
		hour = new Hour();
	}
	
	@Override
	public String toString() {
		return date.toString() + " " + hour.toString();
	}
}
